<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kampus</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-info bg-info text-white">
        <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link text-white" href="#">Home</a>
                <a class="nav-item nav-link active text-white" href="mahasiswa.php">List Mahasiswa <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link text-white" href="prodi.php">List Prodi <span class="sr-only"></span></a>
                <a class="nav-item nav-link text-white" href="about.php">About</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="card mt-3">
            <div class="card-header bg-info text-white">
                Tentang
            </div>
            <div class="card-body">
                <h5 class="card-title ">Sistem informasi Kampus</h5>
                Nama Kelompok :
                <ul>
                    <li>Ihtiara Afrisani - 1931733103</li>
                    <li>Yeni Rahayunengsih - 1931733057</li>
                </ul>
                
            </div>
        </div>
    </div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
